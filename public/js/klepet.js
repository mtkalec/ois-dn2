var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.posljiZasebnoSporocilo = function(podatkiZS){
  
  var secondQuotMarkIndex = 0;
  var firstQuotMarkIndex;
  var tabelaPodatkiZS = new Array();

  for(var i = 0; i < 2; i++) {
      firstQuotMarkIndex = podatkiZS.indexOf('"', secondQuotMarkIndex+1);
      if(firstQuotMarkIndex != -1) {
        secondQuotMarkIndex = podatkiZS.indexOf('"', firstQuotMarkIndex+1);
        
        if(secondQuotMarkIndex != -1) {
            tabelaPodatkiZS.push(podatkiZS.substring(firstQuotMarkIndex+1, secondQuotMarkIndex));
        }
      }
  }

  var sporocilo = {
    podatkiZS: tabelaPodatkiZS 
  }
  
  this.socket.emit('zasebno', sporocilo);
}

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.spremeniKanalZGeslom = function(kanal) {
  var secondQuotMarkIndex = -1;
  var firstQuotMarkIndex;
  var tabelaPodatkiKanala = new Array();

  for(var i = 0; i < 2; i++) {
      firstQuotMarkIndex = kanal.indexOf('"', secondQuotMarkIndex+1);
      if(firstQuotMarkIndex != -1) {
        secondQuotMarkIndex = kanal.indexOf('"', firstQuotMarkIndex+1);
        
        if(secondQuotMarkIndex != -1) {
            tabelaPodatkiKanala.push(kanal.substring(firstQuotMarkIndex+1, secondQuotMarkIndex));
        }
      }
  } 

  this.socket.emit('pridruzitevZaklenjenKanal', {
    podatki: tabelaPodatkiKanala
  });
  
}

Klepet.prototype.procesirajUkaz = function(ukaz) {
  
  var besede = ukaz.split(' ');
  var podatkiZS= ukaz.substring(besede[0].length, ukaz.length);

  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      if(kanal.charAt(0) == "\"") {
        this.spremeniKanalZGeslom(kanal);
        break;
      } 
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      this.posljiZasebnoSporocilo(podatkiZS);  
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};