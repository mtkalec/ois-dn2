var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var geslaKanalov = [];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    pridruzitevZaklenjenKanal(socket);
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPosredovanjeZasebnegaSporocila(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}
  
function aliKanalObstaja(kanal, rooms) {
  for(var k in rooms)
      if(kanal == k) 
        return true;
  return false;
}

function pripraviSeznamUporabnikovGledeNaKanal(socket, kanal) {
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var seznamUporabnikov = new Array();
  for (var i in uporabnikiNaKanalu) {
      var uid = uporabnikiNaKanalu[i].id;
      var u = vzdevkiGledeNaSocket[uid];
      seznamUporabnikov.push(u);
    }
    
    // pošlje le odjemalcu, ki se pridruži kanalu.
    socket.to(kanal).emit('seznamUporabnikov', {
    uporabniki: seznamUporabnikov,
    kanal: kanal
    });

    // pošlje vsem ***ostalim(in ne tistemu, ki se je ravnokar pridružil). ***Slednje obvesti o pridružitvi novega uprabnika v kanal.
    socket.broadcast.to(kanal).emit('seznamUporabnikov', {
    uporabniki: seznamUporabnikov,
    kanal: kanal
    });
}

function pridruzitevZaklenjenKanal(socket) {
  socket.on('pridruzitevZaklenjenKanal', function(zahteva) {
    var podatki = zahteva.podatki;
    var uspesno = 0;
    var kanal;
    var geslo;
    var sporocilo;

    if(podatki.length != 2) {
      uspesno = 1;  // neustrezen format ukaza
    } else {
      kanal = "/" + podatki[0];
      geslo = podatki[1];
      
      var rooms = io.sockets.manager.rooms;
      var kanalObstaja = aliKanalObstaja(kanal, rooms);

      if(!kanalObstaja)       // kanal ne obstaja, kreiraj ga in se mu pridruzi
      {                       
        geslaKanalov[kanal] = geslo;
        pridruzitevKanalu(socket, kanal.substring(1, kanal.length));

      } else if(geslaKanalov[kanal]!= null) 
      {
        if(geslaKanalov[kanal] == geslo)
          pridruzitevKanalaProtokol(socket, kanal.substring(1, kanal.length));
        else 
          uspesno = 2;        // geslo napacno

      } else {
        uspesno = 3;          // kanal prostodostopen
      }

    }

    switch(uspesno) {
      case 1:
        sporocilo = "Neustrezen format, prijavite se lahko s ukazom: /pridruzitev \"<kanal>\" \"<geslo>\"";
        break;
      case 2:
        sporocilo = "Pridružitev v kanal " + kanal.substring(1, kanal.length) + " ni bila uspešna, ker je geslo napačno!";
        break;
      case 3:
        sporocilo = "Izbrani kanal " + kanal.substring(1, kanal.length) + " je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.";
        break;
      default:
        sporocilo = "Uspešno ste se prijavili v zaklenjen kanal " + kanal.substring(1, kanal.length);
    }
      
    socket.emit('pridruzitevZaklenjenKanal', {
      odgovor: sporocilo
    });

  });
    
}

// pomožna funkcija pridruzitevKanalu in pridruzitevZaklenjenKanal
function pridruzitevKanalaProtokol(socket, kanal) {
  socket.join(kanal);
  pripraviSeznamUporabnikovGledeNaKanal(socket, kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);

  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}


function pridruzitevKanalu(socket, kanal) {
  var kanalObstaja = aliKanalObstaja("/"+kanal, io.sockets.manager.rooms);
  // v primeru, da je kanal zaklenjen in se mu želimo pridružititi
  if(kanalObstaja && geslaKanalov["/"+kanal]!=null) {
    socket.emit('pridruzitevZaklenjenKanal', {
      odgovor: "Pridružitev v kanal " + kanal + " ni bila uspešna, ker je geslo napačno. Prijavite se lahko s ukazom:  /pridruzitev \"<kanal>\"  \"<geslo>\" "
    });
    return;
  }

  pridruzitevKanalaProtokol(socket, kanal);

}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeZasebnegaSporocila(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('zasebno', function(tabelaPodatkov){

    var podatkiZS = tabelaPodatkov['podatkiZS'];
    var uspesno = 0;
    // if (napaka == 0) -> brez napak; else -> ustrezna označba napake:
    //  1: neveljaven vnos, 2: prejemnik ne obstaja ali je enak pošiljatelju
    var napaka;       
    var odgovor;    
    
    // preverjanje ustreznosti podatkov
    if(podatkiZS.length != 2) {
        uspesno = 1;
    } else  {
       var posiljatelj = vzdevkiGledeNaSocket[socket.id];
       var prejemnik = podatkiZS[0];
       var sporocilo = podatkiZS[1];
       
       if(posiljatelj == prejemnik || uporabljeniVzdevki.indexOf(prejemnik) == -1) {
          uspesno = 2;
       }
    }

    // iskanje ključa za socket prejemnika
    var key;
    for(var k in vzdevkiGledeNaSocket) {
      if(vzdevkiGledeNaSocket[k] == prejemnik){
        if(vzdevkiGledeNaSocket[k] == prejemnik){
          key = k;
          break;
        }
      }
    }

    // dostop do prejemnikovega objekta socket! DEEELAAAAA ;) 
    // http://stackoverflow.com/questions/6563885/socket-io-how-do-i-get-a-list-of-connected-sockets-clients (pomoč, kako dobiti socket prejemnika
    var socketPrejemnika = io.sockets.sockets[key];

    // za prejmenika (pošlje le v primeru veljavnih podatkov)
    if(uspesno == 0 && socketPrejemnika!=null) {
      odgovor = posiljatelj + "(zasebno): " + sporocilo; 
      socketPrejemnika.emit('zasebno', {
          sporocilo: odgovor
      }); 
    }

    if(uspesno == 0 && socketPrejemnika == null)
      uspesno = 3;

    // ustrezna sporočila za napake.
   if(uspesno == 1) 
      napaka = "Manjkajoči podatki za zasebno sporočilo";
    else if(uspesno == 2 || uspesno == 3) 
      napaka = "Sporočila \"" + sporocilo + "\"uporabniku z vzdevkom \"" + prejemnik + "\" ni bilo mogoče posredovati.";
    

    // za posiljatelja
    if(uspesno == 0 && socketPrejemnika!=null) 
      odgovor = "(zasebno za " + prejemnik + "): " + sporocilo;
    else
      odgovor = napaka;

    socket.emit('zasebno', {
      sporocilo: odgovor
    });

  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pripraviSeznamUporabnikovGledeNaKanal(socket, trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal);
    
    var kanalObstaja = aliKanalObstaja("/"+trenutniKanal[socket.id], io.sockets.manager.rooms);
    if(!kanalObstaja) 
      delete geslaKanalov["/"+trenutniKanal[socket.id]];
    
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}